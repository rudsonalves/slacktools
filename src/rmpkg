#!/bin/env python
#
# rmpkg: this is a python implementation of the removepkg program from
#        pkgtools
#
# feedback: rudsonalves ar rra.etc.br
#
# rmpkg is a free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Fundation; either version 2 of the License, or any later version.
#
# rmpkg is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307, USA
#
__version__ = '1.0.14'
__program_name__ = 'rmpkg'
__project__ = 'slacktools'

__install_dir__ = None

from optparse import OptionParser, Values

from re import compile
from os import listdir, makedirs, remove, chmod, rmdir, getpid, stat, chown
from shutil import move, copy2, copy
from os.path import basename, dirname, isdir, isfile, islink, getmtime as get_modified_time, \
                    getctime as get_created_time, exists
from sys import exit, path

# -------------------------------------------------
# library directory
if __install_dir__:
    lib_dir = '{0}/lib/{1}'.format(__install_dir__, __project__)
else:
    lib_dir = '../libs' # for test only

path.append(lib_dir)

from slacktools_lib import setprocname, getprocname, packages_dir, package_name, \
                           find_package, pkgbase, mask2upgrade
# -------------------------------------------------

root = ''
noquiet = True
preserve = False
warn = False
keep = False
opcopy = False
true_remove = False

pkgs_basedir = packages_dir
adm_dir = '/var/log'
tmp_dir = '{0}/setup/tmp'.format(adm_dir)
pres_dir = '{0}/'.format(tmp_dir)

pid =  getpid()

# -------------------------------
# -      system functions       -
# -------------------------------
# define command parameters
def set_parameters():
    '''set_parameters(): set program initial parameters'''
    global verbose, root, noquiet, preserve, warn, keep, opcopy, true_remove

    parser = OptionParser(usage = 'Use: %prog [options] <PACKAGE_NAME>', \
                          version = '%prog {0}'.format(__version__))

    # Build Actions:
    parser.add_option('-r', '--root', action = 'store', dest = 'root', \
                      default = '', help = 'use a location other than / as root filesystem')
    parser.add_option('-w', '--warn', action = 'store_const', dest = 'warn', \
                      default = False, const = True, help = 'generate  a  report  to  the  standard output about ' + \
                      'which files and directories would be removed, but does not actually remove the package')
    parser.add_option('-p', '--preserve', action = 'store_const', dest = 'preserve', \
                      default = False, const = True, help = 'if specified, the complete package subtree is' + \
                      ' reconstructed in /var/log/setup/tmp/preserved_packages/packagename')
    parser.add_option('-c', '--copy', action = 'store_const', dest = 'copy', \
                      default = False, const = True, help = 'construct a copy of the package under' + \
                      ' /var/log/setup/tmp/preserved_packages/packagename, but don\'t remove it. (same' + \
                      ' effect as --warn --preserve)')
    parser.add_option('-k', '--keep', action = 'store_const', dest = 'keep', \
                      default = False, const = True, help = 'save the intermediate files created by removepkg' + \
                      ' (delete_list, required_files, uniq_list, del_link_list, required_links, required_list).' + \
                      ' Mostly useful for debugging purposes')
    parser.add_option('-q', '--quiet', action = 'store_const', dest = 'quiet', \
                      default = False, const = True, help = 'turn off rmpkg\'s output messages')
    parser.add_option('-t', '--true-remove', action = 'store_const', dest = 'true_remove', \
                      default = False, const = True, help = 'Caution: this option remove all files of a package,' + \
                      ' even if they are present in other package.')

    parser.add_option('--verbose', action = 'store_const', dest = 'verbose', \
                      default = False, const = True, help = 'enable verbose mode')

    conf, args = parser.parse_args()

    # Set program flags
    verbose = conf.__dict__['verbose']
    root = conf.__dict__['root']
    noquiet = not conf.__dict__['quiet']
    preserve = conf.__dict__['preserve']
    warn = conf.__dict__['warn']
    keep = conf.__dict__['keep']
    opcopy = conf.__dict__['copy']
    true_remove = conf.__dict__['true_remove']

    if opcopy:
        warn = True
        preserve = True

    # return args
    return args[:]


# -----------------------------------------------
#                Program Functions
# -----------------------------------------------
def change_basedirs():
    global pkgs_basedir, tmp_dir, adm_dir, pres_dir

    adm_dir = '{0}/var/log'.format(root)
    pkgs_basedir = '{0}/packages'.format(adm_dir)
    tmp_dir = '{0}/setup/tmp'.format(adm_dir)
    pres_dir = '{0}/preserved_packages'.format(tmp_dir)


def cat_except(dir, pkg = None):
    '''cat_except(dir, pkg): returns a list with the contents (without package descriptions) of all
                    packages in dir, except of pkg'''
    ldir = listdir(dir)
    if pkg:
        ldir.remove(pkg)

    lines = []
    if 'scripts' in dir:
        for pkg in ldir:
            sfile = file('{0}/{1}'.format(dir, pkg), 'r')
            lines.extend(sfile.read().split('\n'))
            sfile.close()
    else:
        for pkg in ldir:
            lines.extend(get_package_list_files(pkg))

    lines.sort()
    lines = uniq(lines)

    return lines[:]


def extract_links(slinks):
    '''extract_links(slinks): return a list of links from file or list slinks'''
    if isinstance(slinks, list):
        lines = slinks[:]
    else:
        ffile = file('{0}/scripts/{1}'.format(adm_dir, slinks), 'r')
        lines = ffile.read().split('\n')
        ffile.close()

    mask = compile('^\( *cd ([^ ;][^ ;]*) *; *rm -rf ([^ )][^ )]*) *\) *$')
    elines = []
    for line in lines:
        if mask.match(line):
            elines.append(mask.sub(r'\1/\2', line))

    elines.sort()

    return elines[:]


def preserve_file(parq, pkgname):
    if preserve:
        dir = dirname(parq)
        dest = '{0}/{1}/{2}'.format(pres_dir, pkgname, dir)
        arq = '{0}/{1}'.format(root, parq)
        try:
            if not isdir(dest):
                makedirs(dest)
                return False

            copy2(arq, dest)
            s = stat(arq)
            chmod(dest, s.st_mode)
            chown(dest, s.st_uid, s.st_gid)
        except:
            return False

        return True


def preserve_dir(pdir, pkgname):
    if preserve:
        dir = '{0}/{1}/{2}'.format(pres_dir, pkgname, pdir)
        if not isdir(dir):
            try:
                makedirs(dir)
            except:
                return False

    return True


def keep_files(files_list, pkgname):
    for f in files_list:
        arq = '{0}/{1}'.format(root, f)
        if not isdir(arq):
            if isfile(arq):
                if noquiet: print("  --> {0} was found in another package. Skipping.".format(arq))
                preserve_file(f, pkgname)
            else:
                if f[:8] != 'install/':
                    if noquiet:
                        print("WARNING: Nonexistent {0} was found in another package. Skipping.".format(arq))
        else:
            preserve_dir(f, pkgname)


def unkeep_files(files_list, pkgname):
    for f in files_list:
        arq = '{0}/{1}'.format(root, f)
        if not isdir(arq):
            if isfile(arq):
                if noquiet: print("  --> {0} was found in another package. Removing.".format(arq))
                remove(arq)
            else:
                if f[:8] != 'install/':
                    if noquiet:
                        print("WARNING: Nonexistent {0} was found in another package. Skipping.".format(arq))
        else:
            preserve_dir(f, pkgname)


def keep_links(links_list):
    for l in links_list:
        if islink('{0}/{1}'.format(root, l)):
            if noquiet:
                print("  --> {0}/{1} (symlink) was found in another package. Skipping.".format(root, l))
        else:
            if noquiet:
                print("WARNING: Nonexistent {0}/{1} (symlink) was found in another package. Skipping.".format(root, l))


def unkeep_links(links_list):
    for l in links_list:
        arq = '{0}/{1}'.format(root, l)
        if islink(arq):
            if noquiet:
                print("  --> {0}/{1} (symlink) was found in another package. Removing.".format(root, l))
            remove(arq)
        else:
            if noquiet:
                print("WARNING: Nonexistent {0}/{1} (symlink) was found in another package. Skipping.".format(root, l))


def delete_files(del_list, pkgname):
    for f in del_list:
        arq = '{0}/{1}'.format(root, f)
        if not isdir(arq):
            if exists(arq):
                if is_younger_than(arq, '{0}/packages/{1}'.format(adm_dir, pkgname)):
                    if noquiet: print("WARNING: {0} changed after package installation.".format(arq))
                if not warn:
                    if noquiet: print("  --> Deleting {0}".format(arq))
                    preserve_file(f, pkgname)
                    remove(arq)
                else:
                    if noquiet: print("  --> {0} would be deleted".format(arq))
                    preserve_file(f, pkgname)

            else:
                if noquiet: print("  --> {0} no longer exists. Skipping.".format(arq))
        else:
            preserve_dir(f, pkgname)


def delete_links(links_list):
    for l in links_list:
        link = '{0}/{1}'.format(root, l)
        if islink(link):
            if not warn:
                if noquiet: print("  --> Deleting symlink {0}".format(link))
                remove(link)
            else:
                if noquiet: print("  --> {0} (symlink) would be deleted".format(link))
        else:
            if noquiet: print("  --> {0} (symlink) no longer exists. Skipping.".format(link))


def delete_dirs(dirs_list):
    dirs_list.reverse()
    for d in dirs_list:
        dir = '{0}/{1}'.format(root, d)
        if isdir(dir):
            if not warn:
                if noquiet: print("  --> Deleting empty directory {0}".format(dir))
                try:
                    rmdir(dir)
                except OSError:
                    if noquiet: print("WARNING: Unique directory {0} contains new files".format(dir))
            else:
                if noquiet: print("  --> {0} (dir) would be deleted if empty".format(dir))


def delete_cats(files_list):
    cat_mask = compile(r'(.*/)man(./[^/]*)$')
    for k in files_list:
        if cat_mask.match(k):
            n = cat_mask.sub(r'\1cat\2', k)
            f = '{0}/{1}'.format(root, n)
            if isfile(f):
                if not warn:
                    if noquiet: print("  --> Deleting {0} (fmt man page)".format(f))
                    remove(f)
                else:
                    if noquiet: print("  --> Deleting {0} (fmt man page) would be deleted".format(f))


def listdir_filter(lpath, filter = None):
    lfiles = listdir(lpath)
    if not filter:
        return lfiles[:]

    mask = compile(filter)
    ffile = []
    for f in lfiles:
        if mask.match(f):
            ffile.append(f)

    return ffile[:]


def remove_packages(pkg_list):
    for pkg in pkg_list:
        if noquiet: print('')
        pkg0 = basename(pkg)
        if mask2upgrade.match(pkg0):
            if isfile('{0}/{1}'.format(pkgs_basedir, pkg0)):
                pkgname = pkg0
            else:
                if noquiet: print('Package {0} not found'.format(pkg0))
                continue
        else:
            short = package_name(pkg0)
            try:
                pkgname = find_package(short, root)[0]
            except TypeError:
                if noquiet: print('Package {0} not found'.format(pkg0))
                continue

        if isfile('{0}/{1}'.format(pkgs_basedir, pkgname)):
            if not warn:
                if noquiet: print('Removing package {0}/{1}...'.format(pkgs_basedir, pkgname))
                if noquiet: print('Removing files:')

            delete_list = get_package_list_files(pkgname)
            required_list = cat_except(pkgs_basedir, pkgname)

            if isfile('{0}/scripts/{1}'.format(adm_dir, pkgname)):
                del_link_list = extract_links(pkgname)
                required_links = extract_links(cat_except('{0}/scripts'.format(adm_dir), pkgname))

                required_files = required_list[:]

                required_list.extend(required_links)
                required_list.sort()
                required_list = uniq(required_list)

                uniq_list, none_list, keep_list = comm(del_link_list, required_list)

                if true_remove:
                    unkeep_links(keep_list)
                else:
                    keep_links(keep_list)

                delete_links(uniq_list)
            else:
                required_links = cat_except('{0}/scripts'.format(adm_dir))
                required_files = required_list[:]

                required_list.extend(required_links)
                required_list.sort()
                required_list = uniq(required_list)

            uniq_list, none_list, keep_list = comm(delete_list, required_list)

            if true_remove:
                unkeep_files(keep_list, pkgname)
            else:
                keep_files(keep_list, pkgname)

            delete_files(uniq_list, pkgname)
            delete_dirs(uniq_list)
            delete_cats(uniq_list)

            if keep:
                # save list if keep
                listname = '{0}/delete_list{1}'.format(tmp_dir, pid)
                write_list(listname, delete_list)

                listname = '{0}/required_files{1}'.format(tmp_dir, pid)
                write_list(listname, required_files)

                listname = '{0}/uniq_list{1}'.format(tmp_dir, pid)
                write_list(listname, uniq_list)

                listname = '{0}/del_link_list{1}'.format(tmp_dir, pid)
                write_list(listname, del_link_list)

                listname = '{0}/required_links{1}'.format(tmp_dir, pid)
                write_list(listname, required_links)

                listname = '{0}/required_list{1}'.format(tmp_dir, pid)
                write_list(listname, required_list)

            if preserve:
                if isfile('{0}/scripts/{1}'.format(adm_dir, pkgname)):
                    if not isdir('{0}/{1}/install'.format(pres_dir, pkgname)):
                        makedirs('{0}/{1}/install'.format(pres_dir, pkgname))

                    copy('{0}/scripts/{1}'.format(adm_dir, pkgname), \
                         '{0}/{1}/install/doinst.sh'.format(pres_dir, pkgname))

            if not warn:
                for dir in ('removed_packages', 'removed_scripts'):
                    wdir = '{0}/{1}'.format(adm_dir, dir)
                    if not isdir(wdir):
                        makedirs(wdir)
                        chmod(wdir, 0o755)

                src = '{0}/{1}'.format(pkgs_basedir, pkgname)
                dest = '{0}/removed_packages/{1}'.format(adm_dir, pkgname)

                if isfile(dest): remove(dest)
                move(src, dest)

                src = '{0}/scripts/{1}'.format(adm_dir, pkgname)
                dest = '{0}/removed_scripts/{1}'.format(adm_dir, pkgname)
                if isfile(src):
                    if isfile(dest): remove(dest)
                    move(src, dest)

        else:
            if noquiet: print('No such package: {0}/{1}. Can\'t remove.'.format(pkgs_basedir, pkgname))


def write_list(listname, llist):
    flist = file(listname, 'w')
    for line in llist:
        flist.write('{0}\n'.format(line))

    flist.close()


def is_younger_than(file0, file1):
    if get_modified_time(file0) > get_modified_time(file1):
        return True

    return False


def uniq(old):
   seen = {}
   new = []
   for item in old:
       if item in seen: continue
       seen[item] = 1
       new.append(item)

   return new[:]


def comm(list0, list1, flags = None):
    in_list0 = {}
    in_list1 = {}
    only0 = []
    only1 = []
    in_all = {}
    all_list = []

    # index list0
    for item in list0:
        in_list0[item] = 1

    # index list1
    for item in list1:
        in_list1[item] = 1

    # check if item in list 0 is in list1
    for item in list0:
        if item in in_list1:
            in_all[item] = 1
            all_list.append(item)
        else:
            only0.append(item)

    # check if item in list1 is in list0
    for item in list1:
        if item in in_list0:
            if item in in_all: continue
            in_all[item] = 1
            all_list.append(item)
        else:
            only1.append(item)

    # check flags
    if flags == '12':
        return all_list[:]
    elif flags == '23':
        return only0[:]
    elif flags == '13':
        return only1[:]
    else:
        return only0[:], only1[:], all_list[:]


def get_package_list_files(pkg):
    pfile = file('{0}/{1}'.format(pkgs_basedir, pkg), 'r')
    lines = pfile.read().split('\n')
    pfile.close()

    index = 0
    while lines[index].strip() != 'FILE LIST:':
        index += 1

    lines = lines[index+1:]

    # remove blank lines
    blank = lines.count('')
    for c in range(blank):
        lines.remove('')

    lines.remove('./')
    lines.sort()
    return lines[:]


# -----------------------------------------------
#                Main program
# -----------------------------------------------
if __name__ == '__main__':
    # Set application name
    setprocname(__program_name__)

    # set parameters
    pkgs = set_parameters()

    change_basedirs()

    # If the $TMP directory doesn't exist, create it:
    if not isdir(tmp_dir):
        # remove symlink/file
        try:
            remove(tmp_dir)
        except OSError:
            pass
        makedirs(tmp_dir)
        chmod(tmp_dir, 0o700)

    if warn:
        if noquiet: print('Only warning... not actually removing any files.')

        if preserve:
            if noquiet: print('Package contents is copied to {0}.'.format(pres_dir))

        if noquiet: print('Here\'s what would be removed (and left behind) if you')
        if noquiet: print('removed the package(s):\n')

    elif preserve:
        if noquiet: print('Package contents is copied to {0}.'.format(pres_dir))

    remove_packages(pkgs)

    exit(0)
