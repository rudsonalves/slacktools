#!/bin/env python
#
# SlackTools_lib
#
# feedback: rudsonalves ar rra.etc.br
#
# SlackTools_lib is a free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Fundation; either version 2 of the License, or any later version.
#
# SlackTools_lib is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307, USA
#

__version__ = '0.9.65'
__program_name__ = 'slacktools_lib'
__project__ = 'slacktools'

__install_dir__ = None

from ctypes import cdll, byref, create_string_buffer
from hashlib import md5
from urllib2 import urlopen

from os import chdir, remove, listdir, system, getenv, readlink, read, access, \
    X_OK, getcwd, path as os_path
from os.path import basename, dirname, isdir, isfile, islink, join as path_join
from sys import stdin, stdout
from subprocess import Popen, PIPE, STDOUT
from shlex import split
from random import randrange
from re import compile, match, sub
from threading import Thread
from fnmatch import fnmatch

import termios, math

TERMIOS = termios

stdout_result = 1
stderr_result = 1

packages_dir = '/var/log/packages'

mask2ins_name = compile(r'^([^\-].*)\-([^\-]*)\-([^\-]*)\-([^\-]*)$')
mask2tgz_name = compile(r'^([^\-].*)\-([^\-]*)\-([^\-]*)\-([^\-]*)\.(t[gblx]z)$')
mask2upgrade  = compile(r'^([^\-].*)\-([^\-]*)\-([^\-]*)\-([^\.\-]*)\-upgraded\-.*$')
bmask_pkgname = r'^{0}\-[^\-]*\-([^\-]*)\-[0-9][^\-]*$'
mask_pkginstalled = compile(r'^([^\-].*\-[^\-]*\-([^\-]*)\-[0-9][^\-]*)\.t[gblx]z$')


# Color variables
BLACK  = '\033[{0};30m{1}\033[0m'
RED    = '\033[{0};31m{1}\033[0m'
GREEN  = '\033[{0};32m{1}\033[0m'
YELLOW = '\033[{0};33m{1}\033[0m'
BLUE   = '\033[{0};34m{1}\033[0m'
PURPLE = '\033[{0};35m{1}\033[0m'
CYAN   = '\033[{0};36m{1}\033[0m'
GRAY   = '\033[0;37m{1}\033[0m'
WHITE  = '\033[1;37m{1}\033[0m'
COLOR  = {'BLACK': BLACK, 'RED': RED, 'GREEN': GREEN, 'YELLOW': YELLOW, \
          'BLUE': BLUE, 'CYAN': CYAN, 'GRAY': GRAY, 'WHITE': WHITE}

# -------------------------------------------------------------------
# -                             Classes                             -
# -------------------------------------------------------------------
class attributes(object):
    ''' attributes(dic): create a object with attributes passed in the
    dictionary dic.'''
    def __init__(self, dic = None):
        self.__keys = []
        if dic != None:
            if isinstance(dic, dict) or isinstance(dic, attributes):
                for key, value in dic.items():
                    self.setattr(key, value)
            elif isinstance(dic, tuple) or isinstance(dic, list):
                for key in dic:
                    self.setattr(key, None)

    def setattr(self, key, value):
        if not hasattr(self, key):
            self.__keys.append(key)
        setattr(self, key, value)

    def items(self):
        List = []
        for key in self.__keys:
            List.append((key, getattr(self, key)))
        return List[:]

    def __contains__(self, key):
        return key in self.__keys

    def __len__(self):
        return len(self.__keys)

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        if not hasattr(self, key):
            self.__keys.append(key)
        setattr(self, key, value)

    def keys(self):
        return self.__keys[:]

    def values(self):
        List = []
        for key in self.__keys:
            List.append(getattr(self, key))
        return List[:]

    def __iter__(self):
        return iter(self.__keys)

    def __str__(self):
        l = self.keys()
        l.sort()
        s = ''
        for k in l:
            if isinstance(self[k], str):
                s = s + '[{0}]: "{1}"\n'.format(k, self[k])
            else:
                s = s + '[{0}]: {1}\n'.format(k, self[k])

        return s

    __repr__ = __str__

    def copy(self):
        new_attr = attributes(self)
        return new_attr

    def __eq__(self, other):
        if not isinstance(other, attributes): return False
        skeys = self.keys()
        okeys = other.keys()
        skeys.sort()
        okeys.sort()
        if skeys != okeys: return False
        for k in skeys:
            if self.__dict__[k] != other[k]: return False

        return True

    def __ne__(self, other):
        if not isinstance(other, attributes): return True
        skeys = self.keys()
        okeys = other.keys()
        skeys.sort()
        okeys.sort()
        if skeys != okeys: return True
        for k in skeys:
            if self.__dict__[k] != other[k]: return True

        return False


# ----------------------------------------
# -       Validate Color Function        -
# ----------------------------------------
def validate_color(color = None):
    if COLOR.has_key(color.upper()):
        return COLOR[color.upper()]
    elif color in COLOR.values():
        return color
    else:
        return None


# ----------------------------------------
# -         String Color Class           -
# ----------------------------------------
class str_color(str):
    WARNING  = YELLOW.format(1, '{1}')
    INFO     = BLUE.format(1, '{1}')
    QUESTION = GREEN.format(1, '{1}')
    CRITICAL = RED.format(1, '{1}')
    HEADER   = WHITE
    color = WHITE
    bold  = 1


    def __init__(self, text = None):
        self.text = text


    def __str__(self):
        if self.text:
            return self.color.format(self.bold, self.text)
        else:
            return None

    __repr__ = __str__


    def set_color(self, color = WHITE, bold = None):
        color = validate_color(color)
        if color:
            self.color = color
        if bold != None:
            self.set_bold(bold)


    def set_bold(self, bold = True):
        self.bold = 1 if bold else 0


    def warning(self, text = None):
        if text:
            return self.WARNING.format(1,text)
        return self.WARNING.format(1, self.text)


    def set_warning_color(self, color = None):
        color = validate_color(color)
        if color:
            self.WARNING = color


    def info(self, text = None):
        if text:
            return self.INFO.format(1, text)
        return self.INFO.format(1, self.text)


    def set_info_color(self, color = None):
        color = validate_color(color)
        if color:
            self.INFO = color


    def question(self, text = None):
        if text:
            return self.QUESTION.format(1, text)
        return self.QUESTION.format(1, self.text)


    def set_question_color(self, color = None):
        color = validate_color(color)
        if color:
            self.QUESTION = color


    def critical(self, text = None):
        if text:
            return self.CRITICAL.format(1, text)
        return self.CRITICAL.format(1, self.text)


    def set_critical_color(self, color = None):
        color = validate_color(color)
        if color:
            self.CRITICAL = color


    def header(self, text = None):
        if text:
            return self.HEADER.format(1, text)
        return self.HEADER.format(1, self.text)


    def set_header_color(self, color = None):
        color = validate_color(color)
        if color:
            self.HEADER = color


    # Basic colors strings
    def black(self, text = None, bold = None):
        if bold != None:
            bold = 1 if bold else 0
        else:
            bold = self.bold
        text = text if text else self.text
        return BLACK.format(bold, text)


    def red(self, text = None, bold = None):
        if bold != None:
            bold = 1 if bold else 0
        else:
            bold = self.bold
        text = text if text else self.text
        return RED.format(bold, text)


    def green(self, text = None, bold = None):
        if bold != None:
            bold = 1 if bold else 0
        else:
            bold = self.bold
        text = text if text else self.text
        return GREEN.format(bold, text)


    def yellow(self, text = None, bold = None):
        if bold != None:
            bold = 1 if bold else 0
        else:
            bold = self.bold
        text = text if text else self.text
        return YELLOW.format(bold, text)


    def blue(self, text = None, bold = None):
        if bold != None:
            bold = 1 if bold else 0
        else:
            bold = self.bold
        text = text if text else self.text
        return BLUE.format(bold, text)


    def purple(self, text = None, bold = None):
        if bold != None:
            bold = 1 if bold else 0
        else:
            bold = self.bold
        text = text if text else self.text
        return PURPLE.format(bold, text)


    def cyan(self, text = None, bold = None):
        if bold != None:
            bold = 1 if bold else 0
        else:
            bold = self.bold
        text = text if text else self.text
        return CYAN.format(bold, text)


    def gray(self, text = None, bold = None):
        if bold != None:
            bold = 1 if bold else 0
        else:
            bold = self.bold
        text = text if text else self.text
        return GRAY.format(bold, text)


    def white(self, text = None, bold = None):
        if bold != None:
            bold = 1 if bold else 0
        else:
            bold = self.bold
        text = text if text else self.text
        return WHITE.format(bold, text)


# ----------------------------------------
# -          Print Color Class           -
# ----------------------------------------
class print_color():
    def __init__(self, color = True, bold = False):
        self.bold = bold
        self.color = color
        self.strc = str_color()


    def enable_color(self):
        self.color = True


    def disable_color(self):
        self.color = False


    def enable_bold(self):
        self.bold = True


    def disable_bold(self):
        self.bold = False


    def warning(self, msg):
        if self.color:
            print(self.strc.warning(msg))
        else:
            print(msg)


    def set_warning_color(self, color):
        self.strc.set_warning_color(color)


    def critical(self, msg):
        if self.color:
            print(self.strc.critical(msg))
        else:
            print(msg)


    def set_critical_color(self, color):
        self.strc.set_critical_color(color)


    def info(self, msg):
        if self.color:
            print(self.strc.info(msg))
        else:
            print(msg)


    def set_info_color(self, color):
        self.strc.set_info_color(color)


    def question(self, msg):
        if self.color:
            print(self.strc.question(msg))
        else:
            print(msg)


    def set_question_color(self, color):
        self.strc.set_question_color(color)


    def header(self, msg):
        if self.color:
            print(self.strc.header(msg))
        else:
            print(msg)


    def set_header_color(self, color):
        self.strc.set_header_color(color)


    # ------------        Color prints        -------------
    def gray(self, msg):
        if self.color:
            print(self.strc.gray(msg))
        else:
            print(msg)


    def white(self, msg):
        if self.color:
            print(self.strc.white(msg))
        else:
            print(msg)


    def normal(self, msg):
        print(msg)


    def red(self, msg, bold = None):
        if self.color:
            print(self.strc.red(msg, bold))
        else:
            print(msg)


    def blue(self, msg, bold = None):
        if self.color:
            print(self.strc.blue(msg, bold))
        else:
            print(msg)


    def green(self, msg, bold = None):
        if self.color:
            print(self.strc.green(msg, bold))
        else:
            print(msg)


    def yellow(self, msg, bold = None):
        if self.color:
            print(self.strc.yellow(msg, bold))
        else:
            print(msg)


    def cyan(self, msg, bold = None):
        if self.color:
            print(self.strc.cyan(msg, bold))
        else:
            print(msg)


    def purple(self, msg, bold = False):
        if self.color:
            print(self.strc.purple(msg, bold))
        else:
            print(msg)


# -------------------------------
# -      system functions       -
# -------------------------------
def getkey():
    '''getkey(): one key'''
    fd = stdin.fileno()
    old = termios.tcgetattr(fd)
    new = termios.tcgetattr(fd)
    new[3] = new[3] & ~TERMIOS.ICANON & ~TERMIOS.ECHO
    new[6][TERMIOS.VMIN] = 1
    new[6][TERMIOS.VTIME] = 0
    termios.tcsetattr(fd, TERMIOS.TCSANOW, new)
    c = None
    try:
        c = read(fd, 1)
    finally:
        termios.tcsetattr(fd, TERMIOS.TCSAFLUSH, old)
    return c


def setprocname(newname):
    '''setprocname(newname): set a new proc name to application'''
    libc = cdll.LoadLibrary('libc.so.6')
    buff = create_string_buffer(len(newname) + 1)
    buff.value = newname
    libc.prctl(15, byref(buff), 0, 0, 0)


def getprocname():
    '''getprocname(): get a current python proc name'''
    libc = cdll.LoadLibrary('libc.so.6')
    buff = create_string_buffer(128)
    libc.prctl(16, byref(buff), 0, 0, 0)
    return buff.value


def get_username():
    '''get_username(): return the current username'''
    user = getenv('USER')
    if user in (None, ''):
        user = getenv('LOGNAME')
    return user


def get_user_name(user = None):
    '''get_user_name(): return the current user full name if is defined'''
    pfile = file('/etc/passwd', 'r')
    lines = pfile.read().split('\n')
    pfile.close()

    if user:
      mask = compile(r'^{0}:.*$'.format(user))
    else:
      mask = compile(r'^{0}:.*$'.format(get_username()))

    for line in lines:
        if mask.match(line):
            break
    else:
        return None

    user_name = line.split(':')[4]
    return user_name if user_name != '' else None


def get_homedir():
    '''get_homedir(): return the home directory'''
    return getenv('HOME')


def getMD5(filename):
    '''getMD5(filename): return a md5sum of a file in current directory'''
    f = file(filename, 'r')
    md5sum = md5(f.read()).hexdigest()
    f.close()

    return md5sum


def is_valid_url(url):
    reg = compile(r'^(ht|f)tp://.*/.*\-[0-9].*\.tar\.(gz|bz2|xz)$')
    return True if reg.match(str(url)) else False


def download(url, dest = None):
    '''download(url): download a file in url address, to current directory'''
    if not dest:
        file_name = basename(url)
    else:
        file_name = dest

    if not is_valid_url(url):
        raise Exception, 'Invalid url {0}'.format(url)

    u = urlopen(url)
    meta = u.info()
    file_size = int(meta.getheaders("Content-Length")[0])

    fout = file(file_name, 'wb')
    print ('Donwloading: {0}\nBytes: {1}'.format(file_name, file_size))

    size_download = 0
    block_size = 8*2**10

    while True:
        buffer = u.read(block_size)
        if not buffer:
            break

        size_download += len(buffer)
        fout.write(buffer)

        status = r"%10d  [%3.2f%%]" % (size_download, size_download*100./file_size)
        status = status + chr(8)*(len(status)+1)
        print status,

    fout.close()


def change_dir(new_dir):
    '''change_dir(new_dir): change current dir to new_dir and return True if ok'''
    if isdir(new_dir):
        chdir(new_dir)
        return True

    return False


def remove_backups(dir = '.', verbose = False):
    '''remove_backups(dir, verbose): remove backups files from build directory'''
    ldir = listdir(dir)
    for f in ldir:
        if match(r'^.*\.bak$', f) or match(r'^.*~$', f):
            if verbose: ('Remove {0}'.format(f))
            remove(f)


def get_filelist(dir_name = None, root = ''):
    ''' get_filelist(dir_name): return a list of files, links and directories
    in dir_name '''
    dir = '{0}/{1}'.format(root, dir_name)
    dir = dir.replace('//', '/')
    if not dir:
        return (None, None, None)

    files = []
    links = []
    dirs = []
    dname = dir
    directories = [dir]

    mask = compile(r'^{0}'.format(dname))
    while len(directories) > 0:
        directory = directories.pop()
        for name in listdir(directory):
            fullpath = path_join(directory, name)
            path_name = mask.sub('', fullpath)
            if islink(fullpath):
                link = readlink(fullpath)
                link = mask.sub('', link)
                links.append((path_name, link))
            elif isfile(fullpath):
                files.append(path_name)
            elif isdir(fullpath):
                directories.append(fullpath)
                dirs.append(path_name)
            else:
                print('ERROR: get_filelist, file unknow...')
                exit(1)

    return files[:], links[:], dirs[:]


def is_execfile(fpath):
    '''is_exefile(fpath): return True if a file in fpath is executable'''
    return isfile(fpath) and access(fpath, X_OK)


def is_int(s):
    '''is_int(s): return True if s is a integer'''
    try:
        i = int(s)
    except ValueError:
        return False

    return True


def convert_yes_no(s):
    ''' convert_yes_no(s): convert string YES/NO to True/False '''
    if s.upper() == 'YES':
        return True
    elif s.upper() == 'NO':
        return False
    else:
        print('String "{0}" isn\'t YES/NO string!')
        raise Exception

# ----------------------------------
# -    Package search Functions    -
# ----------------------------------
def find_package(pkg, root = '/'):
    ''' find_package(pkg, root): find a package installed in your system. Return None if the package is not found '''
    pkgname = prepare_pkgname(package_name(basename(pkg)))
    files = listdir('{0}/{1}'.format(root, packages_dir))
    mask = compile(bmask_pkgname.format(pkgname))

    pfind = []
    for f in files:
        if mask.match(f):
            pfind.append(f)

    if len(pfind) == 0:
        return None
    else:
        return pfind[:]


def find_file_pattern(pattern, path):
    ''' find_file_pattern(pattern, path): find a file with match with pattern in path '''
    result = []
    for name in listdir(path):
        if fnmatch(name, pattern):
            result.append(os_path.join(path, name))
    return result


def isinstalled(pkg, root = '/'):
    '''isinstalled(pkg, root): this function return:
        * 0 if package pkg is intalled and is pkg;
        * 1 if package is installed but version, build, ... differs
        * 2 if package is not installed'''

    p = mask_pkginstalled.sub(r'\1', basename(pkg))
    p0 = package_name(p)
    l = find_package(p0, root)
    if l:
        if l[0] == p:
            return 0, p
        else:
            return 1, l[0]
    else:
        return 2, None


def check_installed(pkg, root = '/'):
    '''isinstalled(pkg, root): this function return:
        * 0 if pkg is intalled and is pkg;
        * 1 if pkg is installed but installed version is newer;
        * 2 if pkg is installed but installed build is newer;
        * 3 if pkg is installed but installed version is older;
        * 4 if pkg is installed but installed build is older;
        * 5 if pkg is installed but installed arch is different;
        * 8 if pkg is installed but something is different;
        * 9 if pkg is not installed '''

    p = mask_pkginstalled.sub(r'\1', basename(pkg))
    p0 = package_name(p)
    l = find_package(p0, root)
    p_name, p_version, p_arch, p_build = package_attrs(p)
    if l:
        l = l[0]

        if l == p:
            # package is installed
            return (0, l)

        i_name, i_version, i_arch, i_build = package_attrs(l)

        if i_version > p_version:
            # installed version is newer than the package version
            return (1, l)

        elif i_build > p_build:
            # installed build is greater than the package build
            return (2, l)

        elif i_version < p_version:
            # installed version is older than the package version
            return (3, l)

        elif i_build < p_build:
            # installed build is less than the package build
            return (4, l)

        elif i_arch != p_arch:
            # installed architecture differs from package architecture
            if p_arch in i_arch or i_arch in p_arch:
                return (0, l)
            return (5, l)

        else:
            # installed package differs
            return (8, l)

    else:
        # package is not installed
        return (9, None)


def prepare_pkgname(pkg):
    aux = pkg.replace('+', '\+')
    aux = aux.replace('-', '\-')
    return aux


def replace_wildcards(pkg):
    aux = sub('*', '.*', pkg)
    aux = sub('?', '.', aux)
    return aux

# ----------------------------------
# -      Package informations      -
# ----------------------------------
def get_package_description(pkg, root = None):
    '''get_package_description(pkg): return a list with a package description'''
    if root:
        pkgfile = '{0}{1}/{2}'.format(root, packages_dir, pkg)
    else:
        pkgfile = '{0}/{1}'.format(packages_dir, pkg)
    pfile = file(pkgfile, 'r')

    lines = []
    while True:
        line = pfile.readline()
        if 'FILE LIST:' not in line:
            lines.append(line.replace('\n', ''))
        else:
            break

    pfile.close()
    return lines[:]


def pkgname2install_name(pkg):
    return mask2ins_name.sub(r'\1', pkg)


def pkgname2tgz_name(pkg):
    return mask2tgz_name.sub(r'\1', pkg)


def package_name(pkg):
    aux = basename(pkg)
    if mask2ins_name.match(aux):
        return mask2ins_name.sub(r'\1', aux)
    elif mask2tgz_name.match(pkg):
        return mask2tgz_name.sub(r'\1', aux)
    else:
        return aux


def package_version(pkg):
    if mask2ins_name.match(pkg):
        return mask2ins_name.sub(r'\2', pkg)
    elif mask2tgz_name.match(pkg):
        return mask2tgz_name.sub(r'\2', pkg)
    else:
        return 'malformed'


def package_arch(pkg):
    if mask2ins_name.match(pkg):
        return mask2ins_name.sub(r'\3', pkg)
    elif mask2tgz_name.match(pkg):
        return mask2tgz_name.sub(r'\3', pkg)
    else:
        return 'malformed'


def package_build(pkg):
    if mask2ins_name.match(pkg):
        return mask2ins_name.sub(r'\4', pkg)
    elif mask2tgz_name.match(pkg):
        return mask2tgz_name.sub(r'\4', pkg)
    else:
        return 'malformed'


def package_attrs(pkg):
    pkgname, version, arch, build = (None, None, None, None)

    if mask2upgrade.match(pkg):
        pkgname = mask2upgrade.sub(r'\1', pkg)
        version = mask2upgrade.sub(r'\2', pkg)
        arch = mask2upgrade.sub(r'\3', pkg)
        build = mask2upgrade.sub(r'\4', pkg)

    elif mask2ins_name.match(pkg):
        pkgname = mask2ins_name.sub(r'\1', pkg)
        version = mask2ins_name.sub(r'\2', pkg)
        arch = mask2ins_name.sub(r'\3', pkg)
        build = mask2ins_name.sub(r'\4', pkg)

    elif mask2tgz_name.match(pkg):
        pkgname = mask2tgz_name.sub(r'\1', pkg)
        version = mask2tgz_name.sub(r'\2', pkg)
        arch = mask2tgz_name.sub(r'\3', pkg)
        build = mask2tgz_name.sub(r'\4', pkg)

    return (pkgname, version, arch, build)


def package_extension(pkg):
    if mask2tgz_name.match(pkg):
        return mask2tgz_name.sub(r'\5', pkg)
    else:
        return 'malformed'


def pkgbase(pkg):
    '''pkgbase(pkg): Return a package name that has been stripped of the dirname portion
                     and any of the valid extensions (only)'''
    mask = compile('^(.*)\.t[gblx]z$')
    return mask.sub(r'\1', basename(pkg))


def mount_remove_command(root = '', use_pkgtools = False):
    if use_pkgtools:
        if root != '':
            cmdline = '/usr/bin/env ROOT={0} removepkg'.format(root)
        else:
            cmdline = 'removepkg'

        cmdline += ' {0}'
    else:
        if root != '':
            cmdline = 'rmpkg --root={0}'.format(root)
        else:
            cmdline = 'rmpkg'

        cmdline += ' {0}'

    return cmdline


def mount_install_command(root = '', use_pkgtools = False, fields = 2):
    if use_pkgtools:
        if root != '':
            cmdline = 'installpkg --root {0}'.format(root)
        else:
            cmdline = 'installpkg'

        if fields == 1:
            cmdline += ' {0}'
        else:
            cmdline += ' {0}/{1}'
    else:
        if root != '':
            cmdline = 'instpkg --root={0}'.format(root)
        else:
            cmdline = 'instpkg'

        if fields == 1:
            cmdline += ' {0}'
        else:
            cmdline += ' {0}/{1}'

    return cmdline


def mount_upgrade_command(root = '', use_pkgtools = False, fields = 2):
    if use_pkgtools:
        if root != '':
            cmdline = '/usr/bin/env ROOT={0} upgradepkg'.format(root)
        else:
            cmdline = 'upgradepkg'

        if fields == 1:
            cmdline += ' {0}'
        else:
            cmdline += ' {0}/{1}'
    else:
        if root != '':
            cmdline = 'upgpkg --root={0}'.format(root)
        else:
            cmdline = 'upgpkg'

        if fields == 1:
            cmdline += ' {0}'
        else:
            cmdline += ' {0}/{1}'

    return cmdline


def human_byte(size):
    if size == 0:
        return '0 B'

    units = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")

    index = int(math.floor(math.log(size,1024)))
    p = math.pow(1024, index)
    s = round(size/p, 2)

    return '{0} {1}'.format(s, units[index])



def human_time(t = 0.):
    days = hours = mins = secs = 0
    aux = t

    if aux > 86400: # days
        days = int(aux/86400)
        aux = aux % 86400

    if aux > 3600:
        hours = int(aux/3600)
        aux = aux % 3600

    if aux > 60:
        mins = int(aux/60)
        aux = aux % 60

    secs = float(aux)

    if days:
        return '{0}d:{1}h:{2}m:{3:2.1f}s'.format(days, hours, mins, secs)
    elif hours:
        return '{0}h:{1}m:{2:2.1f}s'.format(hours, mins, secs)
    elif mins:
        return '{0}m:{1:2.1f}s'.format(mins, secs)
    else:
        return '{0:2.1f}s'.format(secs)


def random_n(n = 1):
    ''' random_n(n) return a random number of n digits '''
    if n < 1: n = 1
    return randrange(10**(n-1), 10**n-1)


#
# Execute command routines
#
def stdout_thread(pipe):
    global stdout_result, exec_quiet
    while True:
        out = pipe.stdout.read(1)
        stdout_result = pipe.poll()
        if out == '' and stdout_result is not None:
            break
        if out != '':
            stdout.write(out)
            stdout.flush()


def stderr_thread(pipe):
    global stderr_result
    while True:
        err = pipe.stderr.read(1)
        stderr_result = pipe.poll()
        if err == '' and stderr_result is not None:
            break
        if err != '':
            stdout.write(err)
            stdout.flush()


def exec_command(command, cwd = None, quiet = False):
    ''' exec_command(command, skip, quiet): execute a command line and return the exit code. If quiet is True, the terminal output is suppressed. The lines with skip content is suppressed from output. '''

    if ';' not in command:
        global exec_quiet
        exec_quiet = quiet

        cmd = split(command)

        p = Popen(cmd, stdout = PIPE, stderr = PIPE, cwd = cwd)

        out_thread = Thread(name='stdout_thread', target=stdout_thread, args=(p,))
        err_thread = Thread(name='stderr_thread', target=stderr_thread, args=(p,))
        err_thread.start()
        out_thread.start()
        out_thread.join()
        err_thread.join()

        return stdout_result + stderr_result

    else:
        cdir = getcwd()
        if cwd: chdir(cwd)
        ans = system(command)
        chdir(cdir)

        return ans


def read_conffile(conf_file):
    ''' read_conffile(conf_file): read config file and return a dictionary with variables_names: value elements '''
    fconf = file(conf_file, 'ro')
    lines = fconf.read().split('\n')
    fconf.close()

    out_dict = attributes()
    for line in lines:
        sline = line.strip()
        if '=' not in sline:
            continue
        if sline[0] != '#':
            index = sline.find('=')
            var_name = sline[:index].lower()
            value = sline[index+1:].replace('"','').replace("'", '').strip()

            # convert values
            if value in ('YES', 'NO'):
                value = convert_yes_no(value)
            elif is_int(value):
                value = int(value)

            out_dict[var_name] = value

    return out_dict.copy()
