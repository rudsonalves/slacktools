#!/bin/env python
#
# BuildSBo
#
# feedback: rudsonalves ar rra.etc.br
#
# BuildSBo is a free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Fundation; either version 2 of the License, or any later version.
#
# BuildSBo is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307, USA
#
__version__ = '1.2.62'
__program_name__ = 'buildsbo'
__project__ = 'slacktools'

__install_dir__ = None

from re import compile, sub

from optparse import OptionParser, Values

from sys import exit, path
from os.path import basename, dirname, isdir, isfile
from os import getcwd, makedirs
from shutil import move, copy
from subprocess import call
from shlex import split

# -------------------------------------------------
# library directory
if __install_dir__:
    lib_dir = '{0}/lib/{1}'.format(__install_dir__, __project__)
else:
    lib_dir = '../libs' # for test only

path.append(lib_dir)

from slacktools_lib import setprocname, getprocname, getMD5, download, change_dir, \
    remove_backups, attributes, get_homedir, get_user_name, get_username, human_byte, \
    human_time
# -------------------------------------------------

# sbobuild directories:
#home_dir = get_homedir()
#sbobuilds_dir = None
#builds_dir = None
#sources_dir = None

# templates directory:
#if __install_dir__:
#    templates_dir = '{0}/templates'.format(lib_dir)
#else:
#    templates_dir = '{0}/../templates'.format(getcwd()) # for test only

#current_dir = None
#work_dir = None

# flags
verbose = False
#download_src = False
#backup_file = False

infofile = 'template.info'
infofile_fields = (('PRGNAM', 'pkgname', 'name of application'), \
                   ('VERSION', 'pkgversion', 'version of application'),
                   ('HOMEPAGE', 'homepage', 'homepage of application'), \
                   ('DOWNLOAD', 'download', \
                   'direct download link(s) of application source tarball(s) arch-independent or x86'), \
                   ('MD5SUM', 'md5sum', 'md5sum(s) of the source tarball(s) defined in DOWNLOAD'), \
                   ('DOWNLOAD_x86_64', 'download64', \
                   'direct download link(s) of application source tarball(s), x86_64 only'), \
                   ('MD5SUM_x86_64', 'md5sum64', 'md5sum(s) of the source tarball(s) defined in DOWNLOAD_x86_64'), \
                   ('REQUIRES', 'requires', '%README%'), \
                   ('MAINTAINER', 'maintainer', 'name of SlackBuild script maintainer'), \
                   ('EMAIL', 'email', 'email address of author'), \
                   ('APPROVED', 'approved', ''))

# -------------------------------
# - package source manipulation -
# -------------------------------
def find_pkgname(_pkg_):
    if _pkg_.download:
        src = basename(_pkg_.download)
        return get_pkgname(src)
    elif _pkg_.download64:
        src = basename(_pkg_.download64)
        return get_pkgname(src)

    return None


def get_pkgname(source):
    reg, status = set_package_regExp(source)
    if status: return reg.sub(r'\1', source)

    return source


def get_srcversion(source):
    reg, status = set_package_regExp(source)
    if status: return reg.sub(r'\2', source)

    return source


def get_srcextension(source):
    reg, status = set_package_regExp(source)
    if status: return reg.sub(r'\3', source)

    return source


def set_package_regExp(source):
    reg = compile(r'^(.*)\-([0-9].*)\.tar\.(.*)$')

    return reg, True if reg.match(source) else False


# -----------------------------------------------------
# -          define command parameters                -
# -----------------------------------------------------
def set_parameters_gui():
    '''set_parameters(): set program initial parameters'''
    parser = OptionParser(usage = 'Use: %prog [options] <PACKAGE_NAME>', \
                          version = '%prog {0}'.format(__version__))

    # Others options
    parser.add_option('--download-source', action = 'store_const', dest = 'download_src',  \
                      default = False, const = True, help = 'Download source to current directory')
    parser.add_option('--verbose', '-v', action = 'store_const', dest = 'verbose', \
                      default = False, const = True, help = 'Enable verbose mode')

    conf, args = parser.parse_args()

    # Store values
    options = attributes(conf.__dict__.copy())

    return options.copy(), args[:]


def set_parameters():
    '''set_parameters(): set program initial parameters'''
    parser = OptionParser(usage = 'Use: %prog [options] <PACKAGE_NAME>', \
                          version = '%prog {0}'.format(__version__))

    # Build Actions:
    parser.add_option('--make-infofile', action = 'store_const', dest = 'make_infofile', \
                      default = False, const = True, help = 'Make .info file')
    parser.add_option('--make-slackbuild', action = 'store_const', dest = 'make_slackbuild', \
                      default = False, const = True, help = 'Make .SlackBuild script')
    parser.add_option('--make-readme', action = 'store_const', dest = 'make_readme', \
                      default = False, const = True, help = 'Make README file')
    parser.add_option('--make-doinst', action = 'store_const', dest = 'make_doinst', \
                      default = False, const = True, help = 'Make doinst.sh script')
    parser.add_option('--make-slackdesc', action = 'store_const', dest = 'make_slackdesc', \
                      default = False, const = True, help = 'Make doinst.sh script')
    parser.add_option('--make-sbopack', action = 'store_const', dest = 'make_sbopack', \
                      default = False, const = True, \
                      help = 'Make pkgname.tgz pack to send to SlackBuild.org')

    # Fast Actions:
    parser.add_option('--update-version', action = 'store', dest = 'update_version', \
                      default = None, help = 'Update package by version')
    parser.add_option('--update-source', action = 'store', dest = 'update_source', \
                      default = None, help = 'Update package by source')
    parser.add_option('--update-url', action = 'store', dest = 'update_url', \
                      default = None, help = 'Update package by url')
    parser.add_option('--update-url64', action = 'store', dest = 'update_url64', \
                      default = None, help = 'Update package by url 64 bits')

    # InfoFile parameters
    parser.add_option('--info-pkgname', action = 'store', dest = 'pkgname', \
                      default = None, help = 'Set Package name')
    parser.add_option('--info-pkgversion', action = 'store', dest = 'pkgversion', \
                      default = None, help = 'Set Package version')
    parser.add_option('--info-homepage', action = 'store', dest = 'homepage', \
                      default = None, help = 'Set Package HomePage URL')
    parser.add_option('--info-download', action = 'store', dest = 'download', \
                      default = None, help = 'Set Package source URL')
    parser.add_option('--info-md5sum', action = 'store', dest = 'md5sum', \
                      default = None, help = 'Set source Md5Sum')
    parser.add_option('--info-download64', action = 'store', dest = 'download64', \
                      default = None,  help = 'Set Package source URL 64 bits')
    parser.add_option('--info-md5sum64', action = 'store', dest = 'md5sum64', \
                      default = None, help = 'Set source Md5Sum 64 bits')
    parser.add_option('--info-maintainer', action = 'store', dest = 'maintainer', \
                      default = None, help = 'Set Maintainer Name')
    parser.add_option('--info-email', action = 'store', dest = 'email', \
                      default = None, help = 'Set Maintainer email address')
    parser.add_option('--info-approved', action = 'store', dest = 'approved', \
                      default = None, help = 'Set Approved name')

    # SlackBuild parameters:
    parser.add_option('--sbuild-template', action = 'store', dest = 'template', \
                      default = 'autotools', help = 'Set one SlackBuild template (autotools).' + \
                      ' Valid values are autotools, cmake, perl, python, and rubygem')
    parser.add_option('--sbuild-build', action = 'store', dest = 'build', \
                      default = 1,  help = 'Set BUILD variable default value (1)')
    parser.add_option('--sbuild-tag', action = 'store', dest = 'tag', \
                      default = 'SBo', help = 'Set tag default value (SBo)')
    parser.add_option('--sbuild-compresstype', action = 'store', dest = 'compresstype', \
                      default = 'tgz', help = 'Set package type default compress type (tgz).' + \
                      ' Valid values are tgz, tbz, tlz, and txz')
    parser.add_option('--sbuild-license', action = 'store', dest = 'license', \
                      default = None, help = 'Set SlackBuild license')
    parser.add_option('--sbuild-documentation', action = 'store', dest = 'sb_documentation', \
                      default = '<documentation>', help = 'Set source documentation')
    parser.add_option('--sbuild-disable-man', action = 'store_const', dest = 'disable_man', \
                      default = False, const = True, help = 'remove man code')
    parser.add_option('--sbuild-disable-info', action = 'store_const', dest = 'disable_info', \
                      default = False, const = True, help = 'remove info code')

    # doinst.sh parameters:
    parser.add_option('--doinst-preserve-perms', action = 'store', dest = 'doinst_preserve_perms', \
                      default = None, \
                      help = 'Set a system initialization script file (/etc/rc.d/rc....)')
    parser.add_option('--doinst-full', action = 'store_const', dest = 'doinst_full', \
                      default = False, const = True, help = 'Add a basic doinst.sh')
    parser.add_option('--doinst-config', action = 'store', dest = 'doinst_config', \
                      default = None,  help = 'Set a list of config files')
    parser.add_option('--doinst-schema-install', action = 'store', dest = 'doinst_schema_install', \
                      default = None, help = 'Set a list of Gnome schema files')
    parser.add_option('--doinst-update-desktop-database', action = 'store_const', \
                      dest = 'doinst_update_desktop_database', default = False, const = True)
    parser.add_option('--doinst-update-mime-database', action = 'store_const', \
                      dest = 'doinst_update_mime_database', default = False, const = True)
    parser.add_option('--doinst-icon-theme-cache', action = 'store_const', \
                      dest = 'doinst_icon_theme_cache', default = False,  const = True)

    # slack-desc options:
    parser.add_option('--slackdesc-short',  action = 'store', dest = 'slackdesc_short', \
                      default = None, help = 'Set sort description text to package')
    parser.add_option('--slackdesc-text', action = 'store', dest = 'slackdesc_text', \
                      default = None, help = 'Set slack-desc package description')

    # README options:
    parser.add_option('--readme-text', action = 'store', dest = 'readme_text', \
                      default = None, help = 'Set README text')

    # Others options
    parser.add_option('--download-source', action = 'store_const', dest = 'download_src',  \
                      default = False, const = True, help = 'Download source to current directory')
    parser.add_option('--verbose', '-v', action = 'store_const', dest = 'verbose', \
                      default = False, const = True, help = 'Enable verbose mode')
    parser.add_option('--settings', '-s', action = 'store_const', dest = 'settings', \
                      default = False, const = True, help = 'Show sbobuild settings')

    conf, args = parser.parse_args()

    # Store values
    options = attributes(conf.__dict__.copy())

    return options.copy(), args[:]


# -----------------------------------------------
#                 Build functions
# -----------------------------------------------
def make_infofile(pkg, backup_file = False):
    '''make_infofile(_pkg): build/save infofile editions, process url to remove version, ...'''

    # backup file
    if isfile(infofile):
        if backup_file:
            move(infofile, '{0}.bak'.format(infofile))

    # write infofile
    ifile = file(infofile, 'w')
    if verbose: print('\nInfofile {0}:'.format(infofile))
    for pkey, ikey, default in infofile_fields:
        if pkg[ikey]:
            line = '{0}="{1}"'.format(pkey, pkg[ikey])
        else:
            if ikey not in ('download', 'md5sum', 'download64', 'md5sum64'):
                line = '{0}="{1}"'.format(pkey, default)
            else:
                if pkg.download != None or pkg.download64 != None:
                    line = '{0}="{1}"'.format(pkey, '')
                else:
                    line = '{0}="{1}"'.format(pkey, default)

        ifile.write(line + '\n')
        if verbose: print(line)

    ifile.close()
    print('{0} done...'.format(infofile))


def make_slackdesc(pkg):
    '''make_slackdesc(pkg): build/rebuild slack-desc file'''
    header = "# HOW TO EDIT THIS FILE:\n" + \
        "# The \"handy ruler\" below makes it easier to edit a package description.  Line\n" + \
        "# up the first '|' above the ':' following the base package name, and the '|'\n" + \
        "# on the right side marks the last column you can put a character in.  You must\n" + \
        "# make exactly 11 lines for the formatting to be correct.  It's also\n" + \
        "# customary to leave one space after the ':' except on otherwise blank lines.\n"

    filename = 'slack-desc'
    # backup file
    if isfile(filename):
        if backup_file:
            move(filename, '{0}.bak'.format(filename))

    # build ruler
    ruler = ' '*len(pkg.pkgname) + '|-----handy-ruler'
    ruler = ruler + '-'*(78 - len(ruler)) + '|'

    slackdesc_short = pkg.slackdesc_short if pkg.slackdesc_short else 'short description of app'
    first_line = '{0}: {0} ({1})'.format(pkg.pkgname, slackdesc_short)

    base_line = '{0}:'.format(pkg.pkgname)
    text_len = 79 - len(base_line)

    # Write slack-desc file
    sdfile = file('slack-desc', 'w')
    sdfile.write('{0}\n'.format(header))
    sdfile.write('{0}\n'.format(ruler))
    sdfile.write('{0}\n'.format(first_line))

    if pkg.slackdesc_text:
        lines = process_slackdesc_text(pkg.slackdesc_text, text_len)
    else:
        lines = ['']*10

    for i in range(10):
        sdfile.write('{0}{1}\n'.format(base_line, lines[i]))

    sdfile.close()
    print('slack-desc done...')


def make_doinst(pkg):
    '''make_doinst(pkg): build/rebuild a doinst.sh script'''

    filename = 'doinst.sh'
    # backup file
    if isfile(filename):
        if backup_file:
            move(filename, '{0}.bak'.format(filename))

    dfile = file(filename, 'w')
    if pkg.doinst_config or pkg.doinst_full:
        doinst_config = 'config() {\n' + \
                        '  NEW="$1"\n' + \
                        '  OLD="$(dirname $NEW)/$(basename $NEW .new)"\n' + \
                        '  # If there\'s no config file by that name, mv it over:\n' + \
                        '  if [ ! -r $OLD ]; then\n' + \
                        '    mv $NEW $OLD\n' + \
                        '  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then\n' + \
                        '    # toss the redundant copy\n' + \
                        '    rm $NEW\n' + \
                        '  fi\n' + \
                        '  # Otherwise, we leave the .new copy for the admin to consider...Z\n' + \
                        '}\n\n'
        dfile.write(doinst_config)

    if pkg.doinst_preserve_perms or pkg.doinst_full:
        doinst_preserve_perms = 'preserve_perms() {\n' + \
                                '  NEW="$1"\n' + \
                                '  OLD="$(dirname $NEW)/$(basename $NEW .new)"\n' + \
                                '  if [ -e $OLD ]; then\n' + \
                                '    cp -a $OLD ${NEW}.incoming\n' + \
                                '    cat $NEW > ${NEW}.incoming\n' + \
                                '    mv ${NEW}.incoming $NEW\n' + \
                                '  fi\n' + \
                                '  config $NEW\n' + \
                                '}\n\n'
        dfile.write(doinst_preserve_perms)

    if pkg.doinst_schema_install or pkg.doinst_full:
        doinst_schema_install = 'schema_install() {\n' + \
                                '  SCHEMA="$1"\n' + \
                                '  GCONF_CONFIG_SOURCE="xml::etc/gconf/gconf.xml.defaults" \\\n' + \
                                '  chroot . gconftool-2 --makefile-install-rule \\\n' + \
                                '    /etc/gconf/schemas/$SCHEMA \\\n' + \
                                '    1>/dev/null\n' + \
                                '}\n\n'
        dfile.write(doinst_schema_install)

    if pkg.doinst_config or pkg.doinst_full:
        if pkg.doinst_config:
            for conf in pkg.doinst_config:
                dfile.write('config {0}\n'.format(conf))
        else:
            dfile.write('config /etc/configuration.new\n')
        dfile.write('\n')

    if pkg.doinst_preserve_perms or pkg.doinst_full:
        if pkg.doinst_preserve_perms:
            for script in pkg.doinst_preserve_perms:
                dfile.write('preserve_perms {0}\n'.format(script))
        else:
            dfile.write('preserve_perms etc/rc.d/rc.INIT.new\n')
        dfile.write('\n')

    if pkg.doinst_schema_install or pkg.doinst_full:
        if pkg.doinst_schema_install:
            for schema in pkg.doinst_schema_install:
                dfile.write('schema_install {0}\n'.format(schema))
        else:
            dfile.write('schema_install blah.schemas\n')
        dfile.write('\n')

    if pkg.doinst_update_desktop_database or pkg.doinst_full:
        doinst_update_desktop_database = 'if [ -x /usr/bin/update-desktop-database ]; then\n' + \
                        '  /usr/bin/update-desktop-database -q usr/share/applications >/dev/null 2>&1\n' + \
                        'fi\n\n'
        dfile.write(doinst_update_desktop_database)

    if pkg.doinst_update_mime_database or pkg.doinst_full:
        doinst_update_mime_database = 'if [ -x /usr/bin/update-mime-database ]; then\n' + \
                        '  /usr/bin/update-mime-database usr/share/mime >/dev/null 2>&1\n' + \
                        'fi\n\n'
        dfile.write(doinst_update_mime_database)

    if pkg.doinst_icon_theme_cache or pkg.doinst_full:
        doinst_icon_theme_cache = 'if [ -e usr/share/icons/hicolor/icon-theme.cache ]; then\n' + \
                        '  if [ -x /usr/bin/gtk-update-icon-cache ]; then\n' + \
                        '    /usr/bin/gtk-update-icon-cache usr/share/icons/hicolor >/dev/null 2>&1\n' + \
                        '  fi\n' + \
                        'fi\n\n'
        dfile.write(doinst_icon_theme_cache)

    if pkg.doinst_schema_install or pkg.doinst_full:
        doinst_glib_compile_schemas = 'if [ -x /usr/bin/glib-compile-schemas ]; then\n' + \
                        '  /usr/bin/glib-compile-schemas /usr/share/glib-2.0/schemas/\n' + \
                        'fi\n\n'
        dfile.write(doinst_glib_compile_schemas)

    dfile.close()
    print('doinst.sh done...')


def make_readme(pkg):
    '''make_readme(pkg): build/rebuild a README file'''
    filename = 'README'
    # backup file
    if isfile(filename):
        if backup_file:
            move(filename, '{0}.bak'.format(filename))

    rfile = file(filename, 'w')
    if pkg.readme_text:
        rfile.write(pkg.readme_text)
    else:
        rfile.write('{0}'.format(pkg.pkgname))
        if pkg.slackdesc_short:
            rfile.write(' ({0})\n\n'.format(pkg.slackdesc_short))
        else:
            rfile.write('\n\n')

        if pkg.slackdesc_text:
            rfile.write('{0}\n\n'.format(pkg.slackdesc_text))
        else:
            readme_default = 'The README file should contain at least the following information:\n' + \
                '1. A descripton of the application. The most common way to do this is by including\n' + \
                '   the contents of the slack-desc file (without the "$PRGNAM:" portion), but you\n' + \
                '   may deviate from this if needed. Have a look at some of the other README files\n' + \
                '   to get an idea of what is expected.\n' + \
                '2. Any application and/or library dependencies that are not included in the official\n' + \
                '   Slackware package set; you should also note the home page of the dependencies.\n' + \
                '3. Any other relevant information that might be useful to someone using the script:\n' + \
                '   - Is special configuration needed before building the package?\n' + \
                '   - Is special configuration needed after installing the package?\n' + \
                '   - Is this application incompatible with some other application?\n\n'
            rfile.write('{0}\n\n'.format(readme_default))

        if pkg.homepage:
            rfile.write('{0}\n'.format(pkg.homepage))
        else:
            rfile.write('(take package url here...)\n')

    print('README done...')


def make_slackbuild(pkg, templates_dir):
    slackbuild_file = '{0}.SlackBuild'.format(pkg.pkgname)
    # backup file
    if isfile(slackbuild_file):
        if backup_file:
            move(slackbuild_file, '{0}.bak'.format(slackbuild_file))

    template_file = '{0}/{1}-template.SlackBuild'.format(templates_dir, pkg.template)

    edit_slackbuild(template_file, slackbuild_file, pkg)

    print('{0} done...'.format(slackbuild_file))


def make_sbopack(pkg):
    '''make_sbopack(pkg): make a tgz pack to send to SlackBuild.org. Warning, this option remove all .bak files.'''
    remove_backups('.', verbose)
    change_dir('..')
    tarfilename = '{0}.tar.bz2'.format(pkg.pkgname)
    cmd = 'tar cjvf {0} {1}/'.format(tarfilename, pkg.pkgname)
    status = call(split(cmd))
    if status != 0:
        raise Exception, 'Error: tar file error'

    move(tarfilename, '/tmp/{0}'.format(tarfilename))
    change_dir(pkg.pkgname)

    print('{0} moved to /tmp/{0}...'.format(tarfilename))


# -----------------------------------------------
#                Suport functions
# -----------------------------------------------
def edit_slackbuild(filein, fileout, pkg):
    '''edit_slackbuild(filein, fileout, pkg): open SlackBuild tamplate and edit it with pkg parameters'''
    ifile = file(filein, 'r')
    text = ifile.read()
    ifile.close()

    text = sub(r'\[\[PKGNAME\]\]', pkg.pkgname, text)
    text = sub(r'\[\[VERSION\]\]', pkg.pkgversion, text)
    text = sub(r'\[\[BUILD\]\]', str(pkg.build), text)
    text = sub(r'\[\[TAG\]\]', pkg.tag, text)
    text = sub(r'\[\[PKGTYPE\]\]', pkg.compresstype, text)
    text = sub(r'\[\[EXTENSION\]\]', pkg.src_extension, text)
    text = sub(r'\[\[your name\]\]', pkg.maintainer, text)
    text = sub(r'\[\[your contact information\]\]', pkg.email, text)
    text = sub(r'\[\[documentation\]\]', pkg.sb_documentation, text)
    if pkg.license:
        text = sub(r'\[\[license\]\]', pkg.license, text)
    else:
        text = sub(r'# \[\[license\]\]\n#\n', '', text)

    ofile = file(fileout, 'w')
    ofile.write(text)
    ofile.close()


def process_slackdesc_text(sdtext, tlen):
    words = sdtext.split(' ')
    lines = []
    lines.append('') # first empty line
    count = 0
    for l in range(1, 10):
        # build one line
        line = ''
        if count < len(words):
            while True:
                line = line + ' ' + words[count]
                count += 1
                if count == len(words): break
                if len(line + ' ' + words[count]) > tlen:
                    break

        lines.append(line)

    if count < len(words):
        print('warning: the length of the text is passed to --slackdesc-text is very large!\n')

    return lines[:]


def build_info(_pkg):
    '''build_info(_pkg): process url to remove version, source, pkgname...'''
    global infofile

    pkg = _pkg.copy()
    url = None

    # get md5sum from pkg or set a md5sum from source
    if pkg.download:
        url = pkg.download
        if not pkg.md5sum:
           pkg.md5sum = get_md5sum(pkg.download)

        if pkg.download64:
            pkg.md5sum64 = get_md5sum(pkg.download64)
    elif pkg.download64:
        url = pkg.download64
        if not pkg.md5sum64:
            pkg.md5sum64 = get_md5sum(pkg.download64)

    # take source name from url
    if url:
        source = basename(url)
    else:
        source = ''

    # get pkgname from pkg or source
    if url:
        if not pkg.pkgname:
            pkg.pkgname = get_pkgname(source)

    # if create infofile name
    if not pkg.pkgname:
        infofile = 'template.info'
    else:
        infofile = '{0}.info'.format(pkg.pkgname)

    # get version from pkg or source
    if not pkg.pkgversion:
        if url:
            pkg.pkgversion = get_srcversion(source)
            pkg['src_extension'] = get_srcextension(source)
        else:
            pkg['src_extension'] = 'gz'
    else:
        pkg['src_extension'] = 'gz'

    return pkg.copy()


def get_md5sum(url, sources_dir = '/tmp'):
    source = '{0}/{1}'.format(sources_dir, basename(url))

    if not isfile(source):
        raise Exception, '{0} not found...'.format(source)

    return getMD5(source)


def print_header0():
    print('{0} version {1}\n'.format(__program_name__, __version__))


def print_header(info = {}):
    print_header0()
    print('Package: {0}              Version: {1}'.format(info.pkgname, info.pkgversion))
    print('Url: {0}\nMd5Sum: {1}'.format(info.download, info.md5sum))
    print('Url x86_64: {0}\nMd5Sum x86_64: {1}'.format(info.download64, info.md5sum64))
    print('Maintainer: {0}           Email: {1}'.format(info.maintainer, info.email))
    print('Approved: {0}\nWork dir: {1}'.format(info.approved, getcwd()))
    print('{0}'.format('-'*78))


def print_conf(conffile):
    print('Config File {0}:'.format(conffile))
    fconf = file(conffile, 'r')
    lines = fconf.read()
    print(lines)
    fconf.close()


def read_infofile(_pkg):
    '''read_infofile(pkg): open to read a information file and return a dictionary
    with values'''
    pkg = _pkg.copy()
    if not isfile(infofile):
        raise Exception, 'File {0} not found'.format(infofile)
        return

    info_par = {}
    reg = compile(r'^(.*)=(.*)$')

    ifile = file(infofile, 'r')
    lines = ifile.read().split('\n')
    ifile.close()

    for line in lines:
        key = reg.sub(r'\1', line).strip()
        value = reg.sub(r'\3', line).replace('\'', '').replace('"', '').strip()

        if (key != '') and (key != value):
            info_par[key] = value

    # set info variables
    for pkey, ikey in info_names:
        pkg[pkey] = info_par[ikey]

    return pkg.copy()


def read_conffile(conffile):
    '''read_conffile(conffile): open to read a information file and return a dictionary
    with values'''
    if not isfile(conffile):
        raise Exception, 'File {0} not found'.format(conffile)
        return

    conf_par = {'MAINTAINER': '', 'EMAIL': '', 'SLBDIR': '', 'DOWNLOAD_SRC': False, \
                'COMPRESS_TYPE': 'tgz', 'BACKUP': False}
    reg = compile(r'^(.*)=(.*)$')

    cfile = file(conffile, 'r')
    lines = cfile.read().split('\n')
    cfile.close()

    for line in lines:
        key = reg.sub(r'\1', line).strip()
        value = reg.sub(r'\2', line).replace('\'', '').replace('"', '').strip()

        if (key != '') and (key != value):
            conf_par[key] = value

    conf_par['DOWNLOAD_SRC'] = True if conf_par['DOWNLOAD_SRC'].lower() == 'true' else False
    conf_par['BACKUP'] = True if conf_par['BACKUP'].lower() == 'true' else False

    return conf_par.copy()


def write_conffile(conffile, pkg):
    '''write_conffile(conffile): write a config file'''
    d = dirname(conffile)
    if not isdir(d):
        makedirs(d)

    fconf = file(conffile, 'w')
    fconf.write('MAINTAINER="{0}"\n'.format(pkg.maintainer))
    fconf.write('EMAIL="{0}"\n'.format(pkg.email))
    fconf.write('SLBDIR="{0}"\n'.format(pkg.sbobuilds_dir))
    fconf.write('COMPRESS_TYPE="{0}"\n'.format(pkg.compresstype))
    fconf.write('DOWNLOAD_SRC="{0}"\n'.format(pkg.download_src))
    fconf.write('BACKUP="{0}"\n'.format(pkg.backup))
    fconf.close()

    if not pkg.maintainer or not pkg.email or not pkg.sbobuilds_dir:
        raise Exception, 'Complements the informations in {0} before proceeding'.format(conffile)

