#!/bin/bash

BUILD_DIR=${TMP:='/tmp/build'}
MAKEPKG=${MAKEPKG:='/sbin/makepkg'}
VERSION=`awk '/^VERSION/ { print $3 }' Makefile`
BUILD=${BUILD:='1'}
PROGRAM='slacktools'

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i486 ;;
    arm*) ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *) ARCH=$( uname -m ) ;;
  esac
fi

echo "Build Slackware Package..."
set -x

rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR
make DESTDIR=$BUILD_DIR install
mkdir -p $BUILD_DIR/doc/$PROGRAM
cp doc/* $BUILD_DIR/doc/$PROGRAM/
mkdir $BUILD_DIR/install
cp slack-desc $BUILD_DIR/install/
cd $BUILD_DIR/ && ${MAKEPKG} -c y -l y /tmp/${PROGRAM}-${VERSION}-${ARCH}-${BUILD}.txz
#cd && rm -rf $TMP

