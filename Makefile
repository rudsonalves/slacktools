VERSION = 0.8.13
BUILD = 1
PREFIX = usr/local
ETC = etc
INSTALL = /usr/bin/install
MAKEPKG = /sbin/makepkg
PROGRAM = slacktools
TMP = /tmp/package-$(PROGRAM)
ARCH = noarch

BASE_BIN_DIR = $(PREFIX)/bin
BASE_SBIN_DIR = $(PREFIX)/sbin
BASE_LIB_DIR = $(PREFIX)/lib/$(PROGRAM)

clean:
	@echo "Clean..."
	@find . -name '*~' | xargs rm -f
	@find . -name '*.pyc' | xargs rm -f
	@find . -name 'Ui_*.py' | xargs rm -f
	@find . -type l | xargs rm -f

build_forms:
	@echo "Libraries..."
	@./build_uis
	@cd libs && python -m compileall .

install_programs:
	$(INSTALL) -D --mode=0755 src/buildsbo     $(DESTDIR)/$(BASE_BIN_DIR)/buildsbo
	$(INSTALL) -D --mode=0755 src/lspkg        $(DESTDIR)/$(BASE_BIN_DIR)/lspkg
	$(INSTALL) -D --mode=0755 src/mktagfile    $(DESTDIR)/$(BASE_BIN_DIR)/mktagfile
	$(INSTALL) -D --mode=0755 src/slackup      $(DESTDIR)/$(BASE_SBIN_DIR)/slackup
	$(INSTALL) -D --mode=0755 src/rmpkg        $(DESTDIR)/$(BASE_SBIN_DIR)/rmpkg
	$(INSTALL) -D --mode=0755 src/rmtool       $(DESTDIR)/$(BASE_SBIN_DIR)/rmtool
	$(INSTALL) -D --mode=0755 src/mkpkg        $(DESTDIR)/$(BASE_SBIN_DIR)/mkpkg
	$(INSTALL) -D --mode=0755 src/instpkg      $(DESTDIR)/$(BASE_SBIN_DIR)/instpkg
	$(INSTALL) -D --mode=0755 src/upgpkg       $(DESTDIR)/$(BASE_SBIN_DIR)/upgpkg
	$(INSTALL) -D --mode=0755 src/sbpkg        $(DESTDIR)/$(BASE_SBIN_DIR)/sbpkg
	#$(INSTALL) -D --mode=0755 src/buildsbo_gui $(DESTDIR)/$(BASE_BIN_DIR)/buildsbo_gui

install_libs:
	$(INSTALL) -D --mode=0644 libs/slacktools_lib.py $(DESTDIR)/$(BASE_LIB_DIR)/slacktools_lib.py
	$(INSTALL) -D --mode=0644 libs/slackpainel_lib.py $(DESTDIR)/$(BASE_LIB_DIR)/slackpainel_lib.py
	$(INSTALL) -D --mode=0644 forms/Ui_buildsbo.py $(DESTDIR)/$(BASE_LIB_DIR)/Ui_buildsbo.py
	$(INSTALL) -D --mode=0644 forms/Ui_input_dialog.py $(DESTDIR)/$(BASE_LIB_DIR)/Ui_input_dialog.py
	$(INSTALL) -D --mode=0644 forms/Ui_download_dialog.py $(DESTDIR)/$(BASE_LIB_DIR)/Ui_download_dialog.py
	$(INSTALL) -D --mode=0644 forms/Ui_source_info_dialog.py $(DESTDIR)/$(BASE_LIB_DIR)/Ui_source_info_dialog.py
	$(INSTALL) -D --mode=0644 libs/buildsbo_lib.py $(DESTDIR)/$(BASE_LIB_DIR)/buildsbo_lib.py

install_templates:
	$(INSTALL) -D --mode=0644 templates/autotools-template.SlackBuild $(DESTDIR)/$(BASE_LIB_DIR)/templates/autotools-template.SlackBuild
	$(INSTALL) -D --mode=0644 templates/cmake-template.SlackBuild     $(DESTDIR)/$(BASE_LIB_DIR)/templates/cmake-template.SlackBuild
	$(INSTALL) -D --mode=0644 templates/perl-template.SlackBuild      $(DESTDIR)/$(BASE_LIB_DIR)/templates/perl-template.SlackBuild
	$(INSTALL) -D --mode=0644 templates/python-template.SlackBuild    $(DESTDIR)/$(BASE_LIB_DIR)/templates/python-template.SlackBuild
	$(INSTALL) -D --mode=0644 templates/rubygem-template.SlackBuild   $(DESTDIR)/$(BASE_LIB_DIR)/templates/rubygem-template.SlackBuild

install_docs:
	$(INSTALL) -D --mode=0644 doc/CHANGELOG $(DESTDIR)/$(PREFIX)/doc/$(PROGRAM)/CHANGELOG
	$(INSTALL) -D --mode=0644 doc/COPYING   $(DESTDIR)/$(PREFIX)/doc/$(PROGRAM)/COPYING
	$(INSTALL) -D --mode=0644 doc/README    $(DESTDIR)/$(PREFIX)/doc/$(PROGRAM)/README
	$(INSTALL) -D --mode=0644 doc/TODO      $(DESTDIR)/$(PREFIX)/doc/$(PROGRAM)/TODO

install_conf:
	$(INSTALL) -D --mode=0644 conf/sbpkg.conf $(DESTDIR)/$(ETC)/$(PROGRAM)/sbpkg.conf.new

install: clean
	@echo "Install..."
	make build_forms install_programs install_templates install_docs install_libs install_conf
	./set_instdir.sh $(DESTDIR)/$(BASE_BIN_DIR)/buildsbo $(PREFIX)
	./set_instdir.sh $(DESTDIR)/$(BASE_BIN_DIR)/lspkg $(PREFIX)
	./set_instdir.sh $(DESTDIR)/$(BASE_BIN_DIR)/mktagfile $(PREFIX)
	./set_instdir.sh $(DESTDIR)/$(BASE_SBIN_DIR)/slackup $(PREFIX)
	./set_instdir.sh $(DESTDIR)/$(BASE_SBIN_DIR)/rmpkg $(PREFIX)
	./set_instdir.sh $(DESTDIR)/$(BASE_SBIN_DIR)/rmtool $(PREFIX)
	./set_instdir.sh $(DESTDIR)/$(BASE_SBIN_DIR)/mkpkg $(PREFIX)
	./set_instdir.sh $(DESTDIR)/$(BASE_SBIN_DIR)/instpkg $(PREFIX)
	./set_instdir.sh $(DESTDIR)/$(BASE_SBIN_DIR)/upgpkg $(PREFIX)
	./set_instdir.sh $(DESTDIR)/$(BASE_SBIN_DIR)/sbpkg $(PREFIX)
	#./set_instdir.sh $(DESTDIR)/$(BASE_BIN_DIR)/buildsbo_gui $(PREFIX)

source:
	@rm -rf /tmp/$(PROGRAM)-$(VERSION) 2>/dev/null
	@cp -a ../$(PROGRAM) /tmp/$(PROGRAM)-$(VERSION)
	@rm -rf /tmp/$(PROGRAM)-$(VERSION)/.git*
	@rm -rf /tmp/$(PROGRAM)-$(VERSION)/.kateproject*
	@( cd /tmp; tar cvf $(PROGRAM)-$(VERSION).tar $(PROGRAM)-$(VERSION) )
	@gzip /tmp/$(PROGRAM)-$(VERSION).tar
	@rm -rf /tmp/$(PROGRAM)-$(VERSION)

sbo:
	@rm -rf /tmp/$(PROGRAM) 2>/dev/null
	@mkdir /tmp/$(PROGRAM)
	@cp scripts/* /tmp/$(PROGRAM)/
	@( cd /tmp; tar cvf $(PROGRAM).tar $(PROGRAM) )
	@gzip /tmp/$(PROGRAM).tar

package:
	@echo "Remember to run this option as root!"
	@make source
	@make sbo
	@ln -s /tmp/$(PROGRAM)-$(VERSION).tar.gz /tmp/$(PROGRAM)/.
	@cd /tmp/$(PROGRAM) && . ./$(PROGRAM).SlackBuild
	@cd .. && rm -rf /tmp/$(PROGRAM)
