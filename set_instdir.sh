#!/bin/bash
#
if [ -z "$1" ]; then
  exit 1
fi

echo 

if [ -d $1 ]; then
  for i in $1/*.py; do
    # echo "sed -i 's|^__install_dir__.*\$|__install_dir__ = \"/$2\"|' $i"
    eval "sed -i 's|^__install_dir__.*\$|__install_dir__ = \"/$2\"|' $i"
    echo "Set install directory in $i"
  done
else 
  # echo "sed -i 's|^__install_dir__.*\$|__install_dir__ = \"/$2\"|' $1"
  eval "sed -i 's|^__install_dir__.*\$|__install_dir__ = \"/$2\"|' $1"
  echo "Set install directory in $1"
fi
